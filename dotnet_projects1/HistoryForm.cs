﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dotnet_projects1
{
    public partial class HistoryForm : Form
    {
        private TextBox tb = null;
        public HistoryForm()
        {
            InitializeComponent();
        }

        public void appendTextBox(string s)
        {
            tb = new TextBox();
            tb.Enabled = false;
            tb.Width = this.Width - 30;
            tb.Text = s;
            flowLayoutPanel1.Controls.Add(tb);
        }

        public void readLogFile(StreamReader sr)
        {
            String line;
            while(!sr.EndOfStream)
            {
                line = sr.ReadLine();
                appendTextBox(line);
            }
        }

        private void HistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }
    }
}
