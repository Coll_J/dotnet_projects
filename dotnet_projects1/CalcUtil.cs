﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class CalcUtil
    {
        char[] operators = { '(', ')', '-', '+', '/', '*', '^' };

        bool checkHierarchy(char top, char current)
        {
            if (top == current)
                return false;
            if (top == '^')
                return true;
            if (Array.IndexOf(operators, top) - Array.IndexOf(operators, current) > 1)
                return true;
            return false;
        }
        List<KeyValuePair<bool, double>> toPostfix(string infix)
        {
            List<KeyValuePair<bool, double>> result = new List<KeyValuePair<bool, double>>();
            Stack<char> op = new Stack<char>();

            bool topopd = false;
            bool topopdf = false;
            double f = 10;
            foreach (char c in infix)
            {
                if (operators.Contains(c))
                {
                    topopd = false;
                    topopdf = false;
                    f = 10;
                    switch (c)
                    {
                        case '(':
                            op.Push(c);
                            break;

                        case ')':
                            while (op.Count > 0 && op.Peek() != '(')
                            {
                                char temp = op.Pop();
                                result.Add(new KeyValuePair<bool, double>(true, (double)temp));
                            }
                            if (op.Count > 0)
                                op.Pop();
                            break;

                        default:
                            if (op.Count == 0)
                                op.Push(c);
                            else if (checkHierarchy(op.Peek(), c))
                            {
                                while (op.Count > 0 && op.Peek() != '(')
                                {
                                    char temp = op.Pop();
                                    result.Add(new KeyValuePair<bool, double>(true, (double)temp));
                                }
                                op.Push(c);
                            }
                            else
                                op.Push(c);
                            break;
                    }

                }
                else
                {
                    if (Char.IsLetter(c) && c != '.')
                    {
                        Console.WriteLine("Non-numeric character! Try Again.\n\n");
                        result.Clear();
                        return result;
                    }
                    if(c == '.')
                    {
                        topopdf = true;
                    }
                    else if (topopd)
                    {
                        double temp = result.ElementAt(result.Count - 1).Value;
                        result.RemoveAt(result.Count - 1);
                        if(topopdf)
                        {
                            temp += (double)Char.GetNumericValue(c) / f;
                            f *= 10;
                        }
                        else
                        {
                            temp = temp * 10 + (double)Char.GetNumericValue(c);
                        }
                        result.Add(new KeyValuePair<bool, double>(false, temp));
                    }
                    else
                        result.Add(new KeyValuePair<bool, double>(false, (double)Char.GetNumericValue(c)));
                    topopd = true;
                }
            }

            while (op.Count > 0)
            {
                char temp = op.Pop();
                result.Add(new KeyValuePair<bool, double>(true, (double)temp));
            }
            return result;
        }

        public double calculate(string infix)
        {
            Stack<double> result = new Stack<double>();
            List<KeyValuePair<bool, double>> exp = toPostfix(infix);

            foreach (KeyValuePair<bool, double> val in exp)
            {
                if (!(val.Key))
                {
                    result.Push(val.Value);
                }
                else
                {
                    double numb2 = result.Pop();
                    double numb1 = result.Pop();
                    switch (val.Value)
                    {
                        case '+':
                            result.Push(numb1 + numb2);
                            break;

                        case '-':
                            result.Push(numb1 - numb2);
                            break;

                        case '*':
                            result.Push(numb1 * numb2);
                            break;

                        case '/':
                            result.Push(numb1 / numb2);
                            break;

                        case '^':
                            result.Push(Math.Pow(numb1, numb2));
                            break;
                        default:
                            break;
                    }
                }
            }

            return result.Pop();
        }

        public CalcUtil()
        {
        }

    }
}
