﻿using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    partial class DinoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GamePanel = new System.Windows.Forms.Panel();
            this.trex = new System.Windows.Forms.PictureBox();
            this.startPanel = new System.Windows.Forms.Panel();
            this.anyKey = new System.Windows.Forms.Label();
            this.escLabel = new System.Windows.Forms.Label();
            this.toJump = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.orText = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cactusBig = new System.Windows.Forms.PictureBox();
            this.corona = new System.Windows.Forms.PictureBox();
            this.cactusDouble = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.scoreTimer = new System.Windows.Forms.Timer(this.components);
            this.pausePanel = new System.Windows.Forms.Panel();
            this.pauseSpace = new System.Windows.Forms.Label();
            this.pauseR = new System.Windows.Forms.Label();
            this.pauseEsc = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.userLabel = new System.Windows.Forms.Label();
            this.leaderPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.GamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trex)).BeginInit();
            this.startPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactusBig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.corona)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactusDouble)).BeginInit();
            this.pausePanel.SuspendLayout();
            this.leaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GamePanel
            // 
            this.GamePanel.Controls.Add(this.trex);
            this.GamePanel.Controls.Add(this.startPanel);
            this.GamePanel.Controls.Add(this.pictureBox1);
            this.GamePanel.Controls.Add(this.cactusBig);
            this.GamePanel.Controls.Add(this.corona);
            this.GamePanel.Controls.Add(this.cactusDouble);
            this.GamePanel.Location = new System.Drawing.Point(0, 83);
            this.GamePanel.Name = "GamePanel";
            this.GamePanel.Size = new System.Drawing.Size(800, 235);
            this.GamePanel.TabIndex = 0;
            // 
            // trex
            // 
            this.trex.BackColor = System.Drawing.Color.Transparent;
            this.trex.Image = global::dotnet_projects1.Properties.Resources.running;
            this.trex.Location = new System.Drawing.Point(60, 183);
            this.trex.Name = "trex";
            this.trex.Size = new System.Drawing.Size(40, 43);
            this.trex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.trex.TabIndex = 1;
            this.trex.TabStop = false;
            this.trex.Tag = "player";
            // 
            // startPanel
            // 
            this.startPanel.Controls.Add(this.anyKey);
            this.startPanel.Controls.Add(this.escLabel);
            this.startPanel.Controls.Add(this.toJump);
            this.startPanel.Controls.Add(this.pictureBox3);
            this.startPanel.Controls.Add(this.orText);
            this.startPanel.Controls.Add(this.pictureBox2);
            this.startPanel.Location = new System.Drawing.Point(199, 12);
            this.startPanel.Name = "startPanel";
            this.startPanel.Size = new System.Drawing.Size(415, 167);
            this.startPanel.TabIndex = 2;
            // 
            // anyKey
            // 
            this.anyKey.AutoSize = true;
            this.anyKey.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anyKey.Location = new System.Drawing.Point(58, 116);
            this.anyKey.Name = "anyKey";
            this.anyKey.Size = new System.Drawing.Size(289, 34);
            this.anyKey.TabIndex = 5;
            this.anyKey.Text = "Press any key to start";
            // 
            // escLabel
            // 
            this.escLabel.AutoSize = true;
            this.escLabel.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.escLabel.Location = new System.Drawing.Point(112, 68);
            this.escLabel.Name = "escLabel";
            this.escLabel.Size = new System.Drawing.Size(178, 34);
            this.escLabel.TabIndex = 4;
            this.escLabel.Text = "ESC to Pause";
            // 
            // toJump
            // 
            this.toJump.AutoSize = true;
            this.toJump.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toJump.Location = new System.Drawing.Point(228, 12);
            this.toJump.Name = "toJump";
            this.toJump.Size = new System.Drawing.Size(125, 34);
            this.toJump.TabIndex = 3;
            this.toJump.Text = "To Jump";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::dotnet_projects1.Properties.Resources.pngkey_com_space_png_92872;
            this.pictureBox3.Location = new System.Drawing.Point(147, 11);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(77, 34);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // orText
            // 
            this.orText.AutoSize = true;
            this.orText.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orText.Location = new System.Drawing.Point(101, 17);
            this.orText.Name = "orText";
            this.orText.Size = new System.Drawing.Size(42, 27);
            this.orText.TabIndex = 1;
            this.orText.Text = "OR";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::dotnet_projects1.Properties.Resources.arrow_keys_png_3_transparent;
            this.pictureBox2.Location = new System.Drawing.Point(48, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(47, 44);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox1.Location = new System.Drawing.Point(1, 226);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 8);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // cactusBig
            // 
            this.cactusBig.Image = global::dotnet_projects1.Properties.Resources.obstacle_1;
            this.cactusBig.Location = new System.Drawing.Point(357, 180);
            this.cactusBig.Name = "cactusBig";
            this.cactusBig.Size = new System.Drawing.Size(23, 46);
            this.cactusBig.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.cactusBig.TabIndex = 4;
            this.cactusBig.TabStop = false;
            this.cactusBig.Tag = "obstacle";
            // 
            // corona
            // 
            this.corona.Image = global::dotnet_projects1.Properties.Resources.icons8_coronavirus_48;
            this.corona.Location = new System.Drawing.Point(543, 181);
            this.corona.Name = "corona";
            this.corona.Size = new System.Drawing.Size(48, 48);
            this.corona.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.corona.TabIndex = 3;
            this.corona.TabStop = false;
            this.corona.Tag = "obstacle";
            // 
            // cactusDouble
            // 
            this.cactusDouble.Image = global::dotnet_projects1.Properties.Resources.obstacle_2;
            this.cactusDouble.Location = new System.Drawing.Point(678, 193);
            this.cactusDouble.Name = "cactusDouble";
            this.cactusDouble.Size = new System.Drawing.Size(32, 33);
            this.cactusDouble.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.cactusDouble.TabIndex = 2;
            this.cactusDouble.TabStop = false;
            this.cactusDouble.Tag = "obstacle";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 41);
            this.label1.TabIndex = 1;
            this.label1.Text = "Score: 0";
            // 
            // gameTimer
            // 
            this.gameTimer.Interval = 20;
            this.gameTimer.Tick += new System.EventHandler(this.GameTimerEvent);
            // 
            // scoreTimer
            // 
            this.scoreTimer.Tick += new System.EventHandler(this.ScoreTick);
            // 
            // pausePanel
            // 
            this.pausePanel.Controls.Add(this.pauseSpace);
            this.pausePanel.Controls.Add(this.pauseR);
            this.pausePanel.Controls.Add(this.pauseEsc);
            this.pausePanel.Location = new System.Drawing.Point(185, 52);
            this.pausePanel.Name = "pausePanel";
            this.pausePanel.Size = new System.Drawing.Size(444, 192);
            this.pausePanel.TabIndex = 2;
            // 
            // pauseSpace
            // 
            this.pauseSpace.AutoSize = true;
            this.pauseSpace.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseSpace.Location = new System.Drawing.Point(99, 125);
            this.pauseSpace.Name = "pauseSpace";
            this.pauseSpace.Size = new System.Drawing.Size(248, 34);
            this.pauseSpace.TabIndex = 2;
            this.pauseSpace.Text = "Space to Continue";
            // 
            // pauseR
            // 
            this.pauseR.AutoSize = true;
            this.pauseR.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseR.Location = new System.Drawing.Point(138, 74);
            this.pauseR.Name = "pauseR";
            this.pauseR.Size = new System.Drawing.Size(168, 34);
            this.pauseR.TabIndex = 1;
            this.pauseR.Text = "R to Restart";
            // 
            // pauseEsc
            // 
            this.pauseEsc.AutoSize = true;
            this.pauseEsc.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseEsc.Location = new System.Drawing.Point(50, 17);
            this.pauseEsc.Name = "pauseEsc";
            this.pauseEsc.Size = new System.Drawing.Size(352, 34);
            this.pauseEsc.TabIndex = 0;
            this.pauseEsc.Text = "ESC to return to calculator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(370, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Current Player:";
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Font = new System.Drawing.Font("Microsoft Tai Le", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userLabel.Location = new System.Drawing.Point(343, 20);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(128, 31);
            this.userLabel.TabIndex = 4;
            this.userLabel.Text = "Username";
            // 
            // leaderPanel
            // 
            this.leaderPanel.Controls.Add(this.label3);
            this.leaderPanel.Location = new System.Drawing.Point(642, 6);
            this.leaderPanel.Name = "leaderPanel";
            this.leaderPanel.Size = new System.Drawing.Size(148, 120);
            this.leaderPanel.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Leaderboard";
            // 
            // DinoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.leaderPanel);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pausePanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GamePanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DinoForm";
            this.Text = "DinoForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyIsDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyIsUp);
            this.GamePanel.ResumeLayout(false);
            this.GamePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trex)).EndInit();
            this.startPanel.ResumeLayout(false);
            this.startPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactusBig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.corona)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactusDouble)).EndInit();
            this.pausePanel.ResumeLayout(false);
            this.pausePanel.PerformLayout();
            this.leaderPanel.ResumeLayout(false);
            this.leaderPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel GamePanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox cactusBig;
        private System.Windows.Forms.PictureBox corona;
        private System.Windows.Forms.PictureBox cactusDouble;
        private System.Windows.Forms.PictureBox trex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Timer scoreTimer;
        private System.Windows.Forms.Panel startPanel;
        private System.Windows.Forms.Label anyKey;
        private System.Windows.Forms.Label escLabel;
        private System.Windows.Forms.Label toJump;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label orText;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Panel pausePanel;
        private Label pauseSpace;
        private Label pauseR;
        private Label pauseEsc;
        private Label label2;
        private Label userLabel;
        private Panel leaderPanel;
        private Label label3;
    }
}