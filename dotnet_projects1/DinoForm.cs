﻿using dotnet_projects1.Properties;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class DinoForm : Form
    {
        private bool jumping = false,
                     goingUp = false,
                     gameIsRunning = false,
                     gamePause = false;
        private int jumpSpeed = 10,
                    score = 0,
                    maxHeight = 60,
                    minHeight = 183,
                    obstacleSpeed = 10;
        private CalculatorForm calculator;
        private List<KeyValuePair<string, int>> top_3 = new List<KeyValuePair<string, int>>();
        Random random = new Random();

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            Application.Exit();
            base.OnFormClosed(e);
        }

        public DinoForm(CalculatorForm calc)
        {
            calculator = calc;
            InitializeComponent();
            pausePanel.Hide();
            leaderPanel.Hide();
            Reset();
        }
        public void addLeaders(KeyValuePair<string, int> player)
        {
            if(top_3.Count < 3) top_3.Add(player);
        }

        public void updateLeaderboard()
        {
            int i = 0;
            foreach(KeyValuePair<string, int> player in top_3)
            {
                Label p = new Label();
                p.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                p.Text = player.Key + " - " + player.Value.ToString();
                p.Location = new Point((leaderPanel.Width/2) - (p.Width/2),25 + (p.Height * i));
                leaderPanel.Controls.Add(p);
                i++;
            }
        }

        private void updateScore(string user, int score)
        {
            bool updated = false;
        }
        public void changeUser(string uname)
        {
            userLabel.Text = uname;
            userLabel.Location = new Point((this.Width / 2) - (userLabel.Width/2), 20);
            userLabel.Refresh();
        }
        private void ScoreTick(object sender, EventArgs e)
        {
            score++;
            label1.Text = "Score: " + score;
            label1.Refresh();
            if ((score % 200) == 0)
            {
                obstacleSpeed++;
            }
        }
        private void GameTimerEvent(object sender, EventArgs e)
        {
            if(jumping)
            {
                if(goingUp)
                {
                    trex.Top -= jumpSpeed;
                    if(trex.Top <= maxHeight)
                    {
                        trex.Top = maxHeight;
                        goingUp = false;
                    }
                }
                else
                {
                    trex.Top += jumpSpeed;
                    if (trex.Top >= minHeight)
                    {
                        trex.Top = minHeight;
                        goingUp = true;
                        jumping = false;
                    }
                }
            }

            foreach (Control item in GamePanel.Controls)
            {
                if (item.Tag == "obstacle")
                {
                    item.Left -= obstacleSpeed;
                    if(item.Left < (-item.Width) )
                    {
                        item.Left = this.GamePanel.Width + random.Next(10, 50) + (item.Width * 100);
                    }

                    if (trex.Bounds.IntersectsWith(item.Bounds))
                    {
                        trex.Image = Resources.dead;
                        GameStop();
                        gameIsRunning = false;
                    }
                }
            }

        }

        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            if (gameIsRunning)
            {
                if ((e.KeyCode == Keys.Space || e.KeyCode == Keys.Up) && jumping == false)
                {
                    jumping = true;
                    goingUp = true;
                }
                else if (e.KeyCode == Keys.Escape && gameIsRunning)
                {
                    GameStop();
                    pausePanel.Show();
                    gamePause = true;
                }
            }

            else if (gamePause)
            {
                if(e.KeyCode == Keys.Escape)
                {
                    this.Hide();
                    calculator.Show();
                }
                if(e.KeyCode == Keys.R)
                {
                    pausePanel.Hide();
                    Reset();
                    startPanel.Show();
                }
                if(e.KeyCode == Keys.Space)
                {
                    pausePanel.Hide();
                    GameStart();
                }
                
            }
            
            else if(!gameIsRunning)
            {
                gameIsRunning = true;
                startPanel.Hide();
                GameStart();
            }
        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {

        }

        private void Reset()
        {
            jumping = false;
            score = 0;
            obstacleSpeed = 10;
            label1.Text = "Score: " + score;
            trex.Image = Resources.running;
            trex.Top = 183;
            gameIsRunning = false;
            gamePause = false;
            foreach(Control item in GamePanel.Controls)
            {
                if(item.Tag == "obstacle")
                {
                    item.Left = this.GamePanel.Width + random.Next(10, 20) + (item.Width * 50);
                }
            }
        }

        private void GameStart()
        {
            gameIsRunning = true;
            gameTimer.Start();
            scoreTimer.Start();
        }

        private void GameStop()
        {
            gameIsRunning = false;
            gameTimer.Stop();
            scoreTimer.Stop();
        }
    }
}
