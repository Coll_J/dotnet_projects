﻿using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using dotnet_projects1;

namespace WindowsFormsApp1
{
    public partial class CalculatorForm : Form
    {
        private Form1 f1;
        private HistoryForm hf;
        private bool open_brac = false;
        private CalcUtil util = new CalcUtil();
        private double prev_ans = 0;
        private bool answered = false;
        private char[] op = { '+', '-' , '*', '/', '.', '(', ')'};

        private static string log_file = "log.txt";
        StreamReader sr = new StreamReader(log_file);
        StreamWriter sw;
        public string UName
        {
            get; set;
        }
        public CalculatorForm(Form1 f)
        {
            f1 = f;
            hf = new HistoryForm();
            hf.Hide();
            hf.readLogFile(sr);
            sr.Close();
            sw = File.AppendText(log_file);
            sw.AutoFlush = true;
            UName = f1.UName;
            InitializeComponent();
        }

        public void updateLabel()
        {
            UName = string.Concat(f1.UName, "!");
            label1.Text = string.Concat("Hello, ", UName); //f1.UName;
            label1.Location = new Point(290 - label1.Width, 9);
            label1.Refresh();
        }
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            Application.Exit();
            base.OnFormClosed(e);
        }

        private void mainmenu_button(object sender, System.EventArgs e)
        {
            this.Hide();
            f1.Show();
        }

        private void button_Click(object sender, System.EventArgs e)
        {
            Button button = (Button)sender;
            if(button.Text == "=")
            {
                if (textBox2.Text == "5//**++--")
                {
                    toDino();
                    textBox2.Text = "0";
                    return;
                }
                textBox2.Text = util.calculate(textBox2.Text).ToString();
                sw.WriteLine(textBox2.Text);
                hf.appendTextBox(textBox2.Text);
                answered = true;
                return;
            }

            if (button.Text == "C") textBox2.Text = "0";
            else if (button.Text == "")
            {
                textBox2.Text = textBox2.Text.Remove(textBox2.Text.Length - 1, 1);
                if (textBox2.Text.Length == 0) textBox2.Text = "0";
            }
            else if (button.Text == "( )")
            {
                if (open_brac)
                {
                    textBox2.Text += ')';
                    open_brac = false;
                }
                else
                {
                    textBox2.Text += '(';
                    open_brac = true;
                }
            }
            else if(button.Text == "ANS")
            {
                textBox2.Text += prev_ans.ToString();
            }
            else
            {
                if(answered)
                {
                    if(char.IsDigit(button.Text.ToCharArray()[0]))
                    {
                        prev_ans = double.Parse(textBox2.Text, System.Globalization.CultureInfo.InvariantCulture);
                        textBox1.Text = prev_ans.ToString();
                        textBox2.Text = "";
                    }
                    answered = false;
                }
                if (textBox2.Text == "0") textBox2.Text = "";
                textBox2.Text += button.Text;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if(char.IsDigit(e.KeyChar) || op.Contains(e.KeyChar))
            {
                if (answered)
                {
                    if (char.IsDigit(e.KeyChar))
                    {
                        prev_ans = double.Parse(textBox2.Text, System.Globalization.CultureInfo.InvariantCulture);
                        textBox1.Text = prev_ans.ToString();
                        textBox2.Text = "";
                    }
                    answered = false;
                }
                if (textBox2.Text == "0") textBox2.Text = "";
                textBox2.Text += e.KeyChar.ToString();
            }
            else if(e.KeyChar == (char)Keys.Enter)
            {
                if (textBox2.Text == "5//**++--")
                {
                    toDino();
                    textBox2.Text = "0";
                    return;
                }
                textBox2.Text = util.calculate(textBox2.Text).ToString();
                answered = true;
            }
            else if(e.KeyChar == (char)Keys.Back)
            {
                textBox2.Text = textBox2.Text.Remove(textBox2.Text.Length - 1, 1);
                if (textBox2.Text.Length == 0) textBox2.Text = "0";
            }
        }

        private void button22_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Top left textbox - Shows previous answer\n"
                + "Big textbox - Click to type on it (type numbers, operators,\n\t\t press Enter key to terminate equation)\n"
                + "C - clear outputs\n"
                + "ANS - use previous answer in the expression");
        }

        private void button23_Click(object sender, System.EventArgs e)
        {
            hf.Show();
        }

        private void toDino()
        {
            f1.toDino();
        }
    }

}
