﻿using dotnet_projects1;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private CalculatorForm f2;
        private Graphics g;
        private ColorPickerForm cpf;
        private bool is_drawing;
        private Pen p;
        private UserDB users_db;
        private DinoForm dinoForm;

        private int x = -1, y = -1;
        public Form1()
        {
            InitializeComponent();

            // Drawing components
            g = panel1.CreateGraphics();
            p = new Pen(Color.Black, 5);
            
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            p.StartCap = p.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            cpf = new ColorPickerForm(p);

            // form 2
            f2 = new CalculatorForm(this);

            //dino form
            dinoForm = new DinoForm(f2);

            // users db form
            users_db = new UserDB(this, f2);

        }

        public string UName
        {
            get; set;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Your name is " + UName);
            users_db.addUser(UName);
            this.Hide();
            f2.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UName = textBox1.Text;
            f2.updateLabel();
            dinoForm.changeUser(UName);
        }

        public DinoForm GetDinoForm()
        {
            return this.dinoForm;
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            Application.Exit();
            base.OnFormClosed(e);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            is_drawing = true;
            x = e.X;
            y = e.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(is_drawing)
            {
                g.DrawLine(p, new Point(x, y), e.Location);
                x = e.X;
                y = e.Y;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            is_drawing = false;
            x = y = -1;
        }

        private void enter_pressed(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                MessageBox.Show("Your name is " + UName);
                this.Hide();
                f2.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            users_db.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cpf.Show();
        }

        public void toDino()
        {
            f2.Hide();
            dinoForm.Show();
        }
    }
}
