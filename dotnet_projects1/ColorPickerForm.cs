﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ColorPickerForm : Form
    {
        Pen penny;
        public ColorPickerForm(Pen p)
        {
            penny = p;
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            panel1.BackColor = Color.FromArgb(RedTrackBar.Value, GreenTrackBar.Value, BlueTrackBar.Value);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            penny.Color = panel1.BackColor;
            this.Hide();
        }
    }
}
