﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace dotnet_projects1
{
    public partial class UserDB : Form
    {
        private List<Button> buttonGroup;
        private bool deleteFlag = false;
        private Form1 form1;
        private CalculatorForm calculatorForm;
        private String ConString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename='C:\\Users\\ZK\\source\\repos\\dotnet_projects1\\dotnet_projects1\\Database1.mdf';Integrated Security=True";
        SqlConnection cnn;
        StringBuilder sql;
        private int count;
        public UserDB(Form1 f1, CalculatorForm f2)
        {
            form1 = f1;
            calculatorForm = f2;
            buttonGroup = new List<Button>();
            InitializeComponent();
            InitializeUsers();
        }
        
        private void InitializeUsers()
        {
            sql = new StringBuilder();
            cnn = new SqlConnection(ConString);
            SqlCommand sc;
            SqlDataReader dr;

            // String sql = "Select * from Users";
            sql.Clear();
            sql.Append("Select * from Users order by HighScore desc");
            try
            {
                cnn.Open();
                sc = new SqlCommand(sql.ToString(), cnn);
                dr = sc.ExecuteReader();
                string temp = "";
                while (dr.Read())
                {
                    Button dummy = new Button();
                    dummy.Text = dr.GetValue(1).ToString();
                    dummy.Anchor = AnchorStyles.None;
                    dummy.Location = new Point((panel1.Width - dummy.Width) / 2, buttonGroup.Count * dummy.Height);
                    dummy.Click += new EventHandler(userButton_handler);
                    dummy.Name = dr.GetValue(0).ToString();
                    panel1.Controls.Add(dummy);
                    buttonGroup.Add(dummy);
                    temp = dr.GetValue(0).ToString();
                    form1.GetDinoForm().addLeaders(new KeyValuePair<string, int>(dr.GetValue(1).ToString(), int.Parse(dr.GetValue(2).ToString())));
                }
                count = int.Parse(temp);
                form1.GetDinoForm().updateLeaderboard();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void updatePanelUI()
        {
            int p = 0;
            foreach (Button btn in panel1.Controls.OfType<Button>().ToList())
            {
                btn.Location = new Point((panel1.Width - btn.Width) / 2, p * btn.Height);
                p++;
            }
            panel1.Update();
        }

        public void addUser(string name)
        {
            string id = insertToDatabase(name);
            insertToPanel(id, name);
        }

        private void  insertToPanel(string id, string name)
        {
            Button dummy = new Button();
            dummy.Text = name;
            dummy.Anchor = AnchorStyles.None;
            dummy.Location = new Point((panel1.Width - dummy.Width) / 2, buttonGroup.Count * dummy.Height);
            dummy.Click += new EventHandler(userButton_handler);
            dummy.Name = id;
            panel1.Controls.Add(dummy);
            buttonGroup.Add(dummy);
        }

        private string insertToDatabase(string name)
        {
            string id = "";
            try
            {
                cnn.Open();
                SqlCommand sc;
                count++;
                id = count.ToString("D4");
                sql.Clear();
                sql.Append("Insert into Users (Id, Username) Values ('");
                sql.Append(id);
                sql.Append("', '");
                sql.Append(name);
                sql.Append("')");

                sc = new SqlCommand(sql.ToString(), cnn);
                sc.ExecuteNonQuery();

                cnn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            return id;
        }

        private void rmFromDatabase(string id)
        {
            try
            {
                cnn.Open();
                SqlCommand sc;
                SqlDataReader dr;

                sql.Clear();
                sql.Append("Delete from Users where id = ");
                sql.Append(id);
                sc = new SqlCommand(sql.ToString(), cnn);
                sc.ExecuteNonQuery();

                cnn.Close();
            } catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on which account to delete");
            deleteFlag = true;
        }

        private void userButton_handler(object sender, EventArgs e)
        {
            if(deleteFlag)
            {
                deleteFlag = false;
                DialogResult dialogResult = MessageBox.Show("Are you sure to delete this account?",
                                                            "Delete account",
                                                            MessageBoxButtons.YesNo);
                if(dialogResult == DialogResult.Yes)
                {
                    buttonGroup.Remove((Button)sender);
                    panel1.Controls.Remove((Button)sender);
                    updatePanelUI();
                    rmFromDatabase(((Button)sender).Name);
                    ((Button)sender).Dispose();
                }

            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Use this account?",
                                                            "Choose account",
                                                            MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    form1.UName = ((Button)sender).Text;
                    form1.Hide();
                    this.Hide();
                    calculatorForm.updateLabel();
                    form1.GetDinoForm().changeUser(form1.UName);
                    calculatorForm.Show();
                }
            }
        }
    }
}
